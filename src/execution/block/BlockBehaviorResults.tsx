import React from "react";
import ContentFrame from "../../components/ContentFrame";
import StandardSelectionBoundary from "../../selection/StandardSelectionBoundary";
import SearchResultNavBar from "../../search/SearchResultNavBar";
import ResultHeader from "../../search/ResultHeader";
import PendingResults from "../../search/PendingResults";
import TransactionItem from "../../search/TransactionItem";
import { useFeeToggler } from "../../search/useFeeToggler";
import {totalBehaviorsFormatter, totalTransactionsFormatter} from "../../search/messages";
import {ProcessedBehavior, ProcessedTransaction} from "../../types";
import { PAGE_SIZE } from "../../params";
import BehaviorResultHeader from "../../search/BehaviorResultHeader";
import BehaviorItem from "../../search/BehaviorItem";

type BlockBehaviorResultsProps = {
    page?: ProcessedBehavior[];
    total: number;
    pageNumber: number;
};

const BlockBehaviorResults: React.FC<BlockBehaviorResultsProps> = ({
                                                                             page,
                                                                             total,
                                                                             pageNumber,
                                                                         }) => {
    let number = page?.length;
    return (
        <ContentFrame>
            <SearchResultNavBar
                pageNumber={pageNumber}
    pageSize={PAGE_SIZE}
    total={number}
    totalFormatter={totalBehaviorsFormatter}
    />
    <BehaviorResultHeader/>
    {page ? (
        <StandardSelectionBoundary>
            {page.map((bx) => (
                    <BehaviorItem bx={bx} selectedAddress={bx.from || ""} />
    ))}
        <SearchResultNavBar
            pageNumber={pageNumber}
        pageSize={PAGE_SIZE}
        total={number}
        totalFormatter={totalBehaviorsFormatter}
        />
        </StandardSelectionBoundary>
    ) : (
        <PendingResults />
    )}
    </ContentFrame>
);
};

export default React.memo(BlockBehaviorResults);