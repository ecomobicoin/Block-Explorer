import React, { useContext } from "react";
import { BlockTag } from "@ethersproject/abstract-provider";
import StandardSubtitle from "../../components/StandardSubtitle";
import BlockLink from "../../components/BlockLink";
import NavBlock from "../../components/NavBlock";
import { RuntimeContext } from "../../useRuntime";
import { useLatestBlockNumber } from "../../useLatestBlock";
import { blockTxsURL } from "../../url";
import {BigNumber} from "@ethersproject/bignumber";

type AccountsWithBalanceProps = {
    blockTag: BlockTag;
    coinsCreated: BigNumber;
};

const BlockBehaviorHeader: React.FC<AccountsWithBalanceProps> = ({
                                                                        blockTag,
    coinsCreated,
                                                                    }) => {
    const { provider } = useContext(RuntimeContext);
    const latestBlockNumber = useLatestBlockNumber(provider);

    return (
        <StandardSubtitle>
            <div className="flex items-baseline space-x-1">
                <span>Accounts with balance,</span>
                <span>{parseInt(coinsCreated.toString())} coins created in the last 24 hours</span>
            </div>
        </StandardSubtitle>
    );
};

export default React.memo(BlockBehaviorHeader);
