import React from "react";
import ContentFrame from "../../components/ContentFrame";
import StandardSelectionBoundary from "../../selection/StandardSelectionBoundary";
import SearchResultNavBar from "../../search/SearchResultNavBar";
import ResultHeader from "../../search/ResultHeader";
import PendingResults from "../../search/PendingResults";
import TransactionItem from "../../search/TransactionItem";
import { useFeeToggler } from "../../search/useFeeToggler";
import {totalAccountsFormatter, totalBehaviorsFormatter, totalTransactionsFormatter} from "../../search/messages";
import {ProcessedAccount, ProcessedBehavior, ProcessedTransaction} from "../../types";
import { PAGE_SIZE } from "../../params";
import BehaviorResultHeader from "../../search/BehaviorResultHeader";
import BehaviorItem from "../../search/BehaviorItem";
import AccountsWithBalanceResultHeader from "../../search/AccountsWithBalanceResultHeader";
import AccountWithBalanceItem from "../../search/AccountWithBalanceItem";

type AccountWithBalanceResultsProps = {
    page?: ProcessedAccount[];
    total: number;
    pageNumber: number;
};

const AccountWithBalanceResults: React.FC<AccountWithBalanceResultsProps> = ({
                                                                       page,
                                                                       total,
                                                                       pageNumber,
                                                                   }) => {
    let number = page?.length;
    return (
        <ContentFrame>
            <SearchResultNavBar
                pageNumber={pageNumber}
                pageSize={PAGE_SIZE}
                total={number}
                totalFormatter={totalAccountsFormatter}
            />
            <AccountsWithBalanceResultHeader/>
            {page ? (
                <StandardSelectionBoundary>
                    {page.map((account,index) => (
                        <AccountWithBalanceItem acc={account} number={index}/>
                    ))}
                </StandardSelectionBoundary>
            ) : (
                <PendingResults />
            )}
        </ContentFrame>
    );
};

export default React.memo(AccountWithBalanceResults);