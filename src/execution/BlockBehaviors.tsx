import React, { useMemo, useContext } from "react";
import { useParams } from "react-router";
import { BigNumber } from "@ethersproject/bignumber";
import StandardFrame from "../components/StandardFrame";
import BlockTransactionHeader from "./block/BlockTransactionHeader";
import BlockTransactionResults from "./block/BlockTransactionResults";
import { PAGE_SIZE } from "../params";
import { RuntimeContext } from "../useRuntime";
import {useBlockBehaviors, useBlockTransactions} from "../useErigonHooks";
import { useSearchParams } from "react-router-dom";
import BlockBehaviorHeader from "./block/BlockBehaviorHeader";
import BlockBehaviorResults from "./block/BlockBehaviorResults";

const BlockBehaviors: React.FC = () => {
    const { provider } = useContext(RuntimeContext);
    const params = useParams();

    const [searchParams] = useSearchParams();
    let pageNumber = 1;
    const p = searchParams.get("p");
    if (p) {
        try {
            pageNumber = parseInt(p);
        } catch (err) {}
    }

    const blockNumber = useMemo(
        () => BigNumber.from(params.blockNumber),
        [params.blockNumber]
    );

    const [totalBxs, bxs] = useBlockBehaviors(
        provider,
        blockNumber.toNumber(),
        pageNumber - 1,
        PAGE_SIZE
    );

    document.title = `Block #${blockNumber} Bxns | Otterscan`;

    return (
        <StandardFrame>
            <BlockBehaviorHeader blockTag={blockNumber.toNumber()} />
            <BlockBehaviorResults
                page={bxs}
                total={totalBxs ?? 0}
                pageNumber={pageNumber}
            />
        </StandardFrame>
    );
};

export default BlockBehaviors;
