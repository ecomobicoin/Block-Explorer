import React, { useMemo, useContext } from "react";
import { useParams } from "react-router";
import { BigNumber } from "@ethersproject/bignumber";
import StandardFrame from "../components/StandardFrame";
import { PAGE_SIZE } from "../params";
import { RuntimeContext } from "../useRuntime";
import {
    useAccountsWithBalance,
    useBlockBehaviors,
    useBlockTransactions,
    useCoinsCreatedInLastDay
} from "../useErigonHooks";
import { useSearchParams } from "react-router-dom";
import BlockBehaviorHeader from "./block/BlockBehaviorHeader";
import BlockBehaviorResults from "./block/BlockBehaviorResults";
import AccountsWithBalanceHeader from "./block/AccountsWithBalanceHeader";
import AccountWithBalanceResults from "./block/AccountWithBalanceResults";

const AccountsWithBalance: React.FC = () => {
    const { provider } = useContext(RuntimeContext);
    const [searchParams] = useSearchParams();
    let pageNumber = 1;
    const p = searchParams.get("p");
    if (p) {
        try {
            pageNumber = parseInt(p);
        } catch (err) {}
    }

    const [totalAccs, accs] = useAccountsWithBalance(
        provider,
        150,
    );
    let totalCoinsCreatedIn24H = useCoinsCreatedInLastDay(provider);

    let coins = BigNumber.from(0);
    if (typeof totalCoinsCreatedIn24H == undefined) {
        coins = BigNumber.from(0);
    }
    document.title = `AccountsWithBalance`;

    return (
        <StandardFrame>
            <AccountsWithBalanceHeader blockTag={1} coinsCreated={coins}/>
            <AccountWithBalanceResults
                page={accs!}
                total={totalAccs ?? 0}
                pageNumber={pageNumber}
            />
        </StandardFrame>
    );
};

export default AccountsWithBalance;
