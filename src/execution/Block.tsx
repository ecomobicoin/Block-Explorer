import React, { useEffect, useMemo, useContext } from "react";
import { useParams, NavLink } from "react-router-dom";
import { commify, formatUnits } from "@ethersproject/units";
import { BigNumber } from "@ethersproject/bignumber";
import { toUtf8String, Utf8ErrorFuncs } from "@ethersproject/strings";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBurn } from "@fortawesome/free-solid-svg-icons";
import StandardFrame from "../components/StandardFrame";
import StandardSubtitle from "../components/StandardSubtitle";
import NavBlock from "../components/NavBlock";
import ContentFrame from "../components/ContentFrame";
import BlockNotFound from "../components/BlockNotFound";
import InfoRow from "../components/InfoRow";
import Timestamp from "../components/Timestamp";
import BlockReward from "./components/BlockReward";
import RelativePosition from "../components/RelativePosition";
import PercentageBar from "../components/PercentageBar";
import BlockLink from "../components/BlockLink";
import DecoratedAddressLink from "./components/DecoratedAddressLink";
import NativeTokenAmount from "../components/NativeTokenAmount";
import FormattedBalance from "../components/FormattedBalance";
import NativeTokenPrice from "../components/NativeTokenPrice";
import HexValue from "../components/HexValue";
import { RuntimeContext } from "../useRuntime";
import { useLatestBlockNumber } from "../useLatestBlock";
import {blockBxsURL, blockTxsURL, blockURL} from "../url";
import { useBlockData } from "../useErigonHooks";
import { useChainInfo } from "../useChainInfo";

const Block: React.FC = () => {
  const { provider } = useContext(RuntimeContext);
  const { blockNumberOrHash } = useParams();
  if (blockNumberOrHash === undefined) {
    throw new Error("blockNumberOrHash couldn't be undefined here");
  }
  const {
    nativeCurrency: { name, symbol },
  } = useChainInfo();

  const block = useBlockData(provider, blockNumberOrHash);
  useEffect(() => {
    if (block !== undefined) {
      document.title = `Block #${blockNumberOrHash} | Otterscan`;
    }
  }, [blockNumberOrHash, block]);

  const extraStr = useMemo(() => {
    return block && toUtf8String(block.extraData, Utf8ErrorFuncs.replace);
  }, [block]);
  const burntFees =
    block?.baseFeePerGas && block.baseFeePerGas.mul(block.gasUsed);
  const gasUsedPerc =
    block && block.gasUsed.mul(10000).div(block.gasLimit.add(BigNumber.from(1))).toNumber() / 100;
  //TODO : handle this division by 0 differently ??

  const latestBlockNumber = useLatestBlockNumber(provider);

  return (
    <StandardFrame>
      <StandardSubtitle>
        <div className="flex items-baseline space-x-1">
          <span>Block</span>
          <span className="text-base text-gray-500">#{blockNumberOrHash}</span>
          {block && (
            <NavBlock
              entityNum={block.number}
              latestEntityNum={latestBlockNumber}
              urlBuilder={blockURL}
            />
          )}
        </div>
      </StandardSubtitle>
      {block === null && (
        <BlockNotFound blockNumberOrHash={blockNumberOrHash} />
      )}
      {block && (
        <ContentFrame>
          <InfoRow title="Block Height">
            <span className="font-bold">{commify(block.number)}</span>
          </InfoRow>
          <InfoRow title="Timestamp">
            <Timestamp value={block.timestamp} />
          </InfoRow>
          <InfoRow title="Transactions">
            <NavLink
              className="rounded-lg bg-link-blue/10 px-2 py-1 text-xs text-link-blue hover:bg-link-blue/100 hover:text-white"
              to={blockTxsURL(block.number)}
            >
              {block.transactionCount} transactions
            </NavLink>{" "}
            in this block
          </InfoRow>
          <InfoRow title="Behaviors">
            <NavLink
                className="rounded-lg bg-link-blue/10 px-2 py-1 text-xs text-link-blue hover:bg-link-blue/100 hover:text-white"
                to={blockBxsURL(block.number)}
            >
              {block.behaviorCount} behaviors
            </NavLink>{" "}
            in this block
          </InfoRow>
          <InfoRow title="Mined by">
            <DecoratedAddressLink address={block.miner} miner />
          </InfoRow>
          <InfoRow title="Block Reward">
            <BlockReward block={block} />
          </InfoRow>
          <InfoRow title="Behavior quantity">
            {commify(block.behaviorTotalQuantity.toString())}
          </InfoRow>
          <InfoRow title="Size">{commify(block.size)} bytes</InfoRow>
          {burntFees && (
            <InfoRow title="Burnt Fees"> //TODO add burnt fees
              <div className="flex items-baseline space-x-1">
                <span className="flex space-x-1 text-orange-500">
                  <span title="Burnt fees">
                    <FontAwesomeIcon icon={faBurn} size="1x" />
                  </span>
                  <span>
                    <span className="line-through">
                      <FormattedBalance value={burntFees} />
                    </span>{" "}
                    {symbol}
                  </span>
                </span>
              </div>
            </InfoRow>
          )}
          <InfoRow title="Extra Data">
            {extraStr} (Hex:{" "}
            <span className="break-all font-data">{block.extraData}</span>)
          </InfoRow>
          <InfoRow title="Difficulty">
            {commify(block._difficulty.toString())}
          </InfoRow>
          <InfoRow title="VDF Difficulty">
            {commify(block.totalDifficulty.toString())}
          </InfoRow>
          <InfoRow title="Hash">
            <HexValue value={block.hash} />
          </InfoRow>
          <InfoRow title="Parent Hash">
            <BlockLink blockTag={block.parentHash} />
          </InfoRow>
          <InfoRow title="Sha3Uncles">
            <HexValue value={block.sha3Uncles} />
          </InfoRow>
          <InfoRow title="StateRoot">
            <HexValue value={block.stateRoot} />
          </InfoRow>
          <InfoRow title="Nonce">
            <span className="font-data">{block.nonce}</span>
          </InfoRow>
        </ContentFrame>
      )}
    </StandardFrame>
  );
};

export default Block;
