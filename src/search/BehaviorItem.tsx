import React from "react";
import BlockLink from "../components/BlockLink";
import TimestampAge from "../components/TimestampAge";
import {ProcessedBehavior} from "../types";
import { BlockNumberContext } from "../useBlockTagContext";
import DecoratedAddressLink from "../execution/components/DecoratedAddressLink";
import InfoRow from "../components/InfoRow";
import {BehaviorReceipt} from "../EthersProjectFork";

type BehaviorItemProps = {
    bx: ProcessedBehavior | BehaviorReceipt;
    selectedAddress?: string;
};

const BehaviorItem: React.FC<BehaviorItemProps> = ({
                                                             bx,
                                                         }) => {

    return (
        <BlockNumberContext.Provider value={bx.blockNumber}>
            <div
                className={`grid grid-cols-12 items-baseline gap-x-1 border-t border-gray-200 text-sm px-2 py-3`}
            >
                <BlockLink blockTag={bx.blockNumber} />

                <TimestampAge timestamp={Number(bx.timestamp)} />
                <span className="col-span-2 flex items-baseline justify-between space-x-2 pr-2">
                    <InfoRow title="">
                        <DecoratedAddressLink address={bx.from!} />
                    </InfoRow>
                </span>
                    <span>{Number(bx.quantity)}</span>
            </div>
        </BlockNumberContext.Provider>
    );
};

export default BehaviorItem;
