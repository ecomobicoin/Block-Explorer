import React from "react";
import { FeeDisplay } from "./useFeeToggler";

export type AccoutsWithBalanceProps = {
};
//TODO : add total coins created in this header
const AccoutsWithBalanceResultHeader: React.FC<AccoutsWithBalanceProps> = () => (
    <div className="grid grid-cols-12 gap-x-1 border-t border-b border-gray-200 bg-gray-100 px-2 py-2 text-sm font-bold text-gray-500">
        <div className="col-span-2 ml-8">Address</div>
        <div className="col-span-2">Quantity</div>
    </div>
);

export default React.memo(AccoutsWithBalanceResultHeader);
