import { commify } from "@ethersproject/units";

export const totalTransactionsFormatter = (total: number) =>
  `A total of ${commify(total)} ${
    total > 1 ? "transactions" : "transaction"
  } found`;

export const totalBehaviorsFormatter = (total: number) =>
    `A total of ${commify(total)} ${
        total > 1 ? "behaviors" : "behavior"
    } found`;

export const totalAccountsFormatter = (total: number) =>
    `A total of ${commify(total)} ${
        total > 1 ? "accounts" : "account"
    } found`;

export const totalContractsFormatter = (total: number) =>
  `A total of ${commify(total)} ${total > 1 ? "contracts" : "contract"} found`;
