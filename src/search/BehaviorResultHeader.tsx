import React from "react";
import { FeeDisplay } from "./useFeeToggler";

export type ResultHeaderProps = {
};

const BehaviorResultHeader: React.FC<ResultHeaderProps> = () => (
    <div className="grid grid-cols-12 gap-x-1 border-t border-b border-gray-200 bg-gray-100 px-2 py-2 text-sm font-bold text-gray-500">
        <div>Block</div>
        <div>Age</div>
        <div className="col-span-2 ml-1">From</div>
        <div className="col-span-2">Quantity</div>
    </div>
);

export default React.memo(BehaviorResultHeader);
