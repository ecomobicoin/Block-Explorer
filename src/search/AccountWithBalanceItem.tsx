import React from "react";
import BlockLink from "../components/BlockLink";
import TimestampAge from "../components/TimestampAge";
import {ProcessedAccount, ProcessedBehavior} from "../types";
import { BlockNumberContext } from "../useBlockTagContext";
import DecoratedAddressLink from "../execution/components/DecoratedAddressLink";
import InfoRow from "../components/InfoRow";
import {BehaviorReceipt} from "../EthersProjectFork";

type AccountWithBalanceItemProps = {
    acc: ProcessedAccount;
    number:number;
};

const AccountWithBalanceItem: React.FC<AccountWithBalanceItemProps> = ({
                                                       acc,
                                                                           number,
                                                   }) => {

    return (
            <div
                className={`grid grid-cols-12 items-baseline gap-x-1 border-t border-gray-200 text-sm px-2 py-4`}
            >
                    <InfoRow title={number + 1}>
                        <DecoratedAddressLink address={acc.address} miner />
                    </InfoRow>
                <span>{parseInt(acc.balance.toString())}</span>
            </div>
    );
};

export default AccountWithBalanceItem;
